<?php
  
  class Bank {
    public $accno;
    public $name;
    public $balance;

    function depositAmount($amt) {
      $this->balance += $amt;
      echo "Total balance for {$this->name} after deposit is now {$this->balance}.<br>";
    }

    function deductAmount($amt) {
      $this->balance -= $amt;
      echo "Total balance for {$this->name} after deduction is now: {$this->balance}.<br>";
    }

    function checkBalance() {
      echo "Balance for {$this->name} is {$this->balance}.<br>";
    }
  }

$user = new Bank();
$user->accno = 101;
$user->name = "Smith, John";
$user->balance = 100.00;
$user->checkBalance();
$user->depositAmount(20.00);
$user->deductAmount(30.00);
$user->checkBalance();

?>
